#CSS 
## What is CSS? : CSS stands for Cascading Style Sheets
It is a language that describes the style of an HTML document.
CSS describes how the HTML elements should be displayed.

CSS will give  the HTML _“Style”_ the style—page layouts, colors, and font.
Make it look pretty.

## How does it work?
You can use it through an _**external style sheet**_ by adding a link to your HML file:

example:
```

<head>
 <link rel=”stylesheet”  type=”text/css”  href=mysitestyle.css”>
 </head>
 
```
 or 
_**internal style sheets**_ that are CSS instructions written directly into the header of a specific .html page. If you
 have a single page on a site that has a unique look. 
 
 Example:
```
 <head>
 
<style>

Body  {  background-color:thistle;  }

P  {  font-size:20px;  color:mediumblue;  }

</style>

 </head>
 ```
